/**
	This module provides behavior tree operations inspired by

	A. Marzinotto and M. Colledanchise and C. Smith and P. Ögren, Towards a unified behavior trees framework for robot
	control, 
	
	and 
	
	Colledanchise, M; Ögren, P, How Behavior Trees Modularize Hybrid Control Systems and Generalize Sequential Behavior
	Compositions, the Subsumption Architecture, and Decision Trees

	The behavior tree simplifies and modularize complex state machine description by redefining nodes as tasks. Doing
	so, the representation becomes a tree, whereas the state machine is generally a more complex graph.
	
	The basic idea is the following: 
	There is a predefined well-known tree-internal ternary language. The decisions inside behavior tree are based on 
	this language. As a consequence, the importer of this package has to adopt the Ternary type inside his own code. 
	
	There are three kinds of nodes: root, intermediates, leafs. 
	A rooted, directed tree is defined, where the root has only one child. 
	Leaf nodes have no children but the user objects. User objects overload the function call operator. During the call
	of the root, all inputs are passed down to the leaves of the tree and then further on, to the user defined objects
	as input params of the opCall method. 
	
	The result of the opCall method is of Ternary type, analogue to the cited papers. 

*/

/** Authors: Alexander Orlov */
/** Copyright: Copyright (c) 2016- Alexander Orlov. All rights reserved. */
/** License: https://opensource.org/licenses/MIT, MIT License. */
module behaviortree; 
import std.typecons;  
import std.functional; 
import std.traits; 

///
debug
{
	void process(T, U...)(auto ref T a, auto ref U args) @nogc
	{
		args[0] *= 10; 
	}

	struct Functor
	{
		size_t value; 
		alias value this; 
		auto opCall(U, T...)(auto ref U callable, auto ref T args) @nogc
		{
			return callable(this, forward!(input[1..$]));
		}
	}

	struct Leaf(T)
	{
		auto funn(U, V...)(auto ref U value, auto ref V args) @nogc
		{
			foreach(ref a; args)
				value += a; 
		}

		T opCall(U, A...)(auto ref U F, auto ref A args) @nogc 
		{
			funn(forward!F, forward!args); 
			static assert(is(T == bool) || is(T == Ternary)); 
			static if(is(T == bool))
			{
				if(F < 50)
					return false; 
				else
					return true; 
			}
			else
			{
				if(F < 50)
					return Ternary.no; 
				else if(F == 50)
					return Ternary.unknown; 
				else
					return Ternary.yes; 
			}
		}
	}

	auto Not(T, U...)(auto ref T member, auto ref U input) @nogc
	{
		auto res = member(forward!input); 
		static if(is(typeof(res) == Ternary))
		{
			if(res == Ternary.no)
				return Ternary.yes; 
			else if(res == Ternary.yes)
				return Ternary.no; 
			else 
				return res; 
		}
		else
		{
			return !res; 
		}
	}
}

/**
	The action node. Executes T with provided arguments, reports the result. If the result is neither true or false, 
	proc is executed with the instance of T and arguments provided. 
*/
struct Action(T, alias proc) 
{
	T member; 

	/// the (...) serves as the tick method of the node
	auto opCall(U...)(auto ref U args)
	{
		import std.meta; 
		auto res = member(forward!args);
		static if(__traits(compiles, proc.init)) proc p;
		if(res == Ternary.unknown)
		{
			static if(isCallable!proc)
			{
				proc(member, forward!args); 
			}
			else
			{
				static if(__traits(isTemplate, proc))
					proc!(T, U)(member, forward!args); 
				else
					p(member, forward!args);
			}

		}	
		return res; 
	}
}

///
@nogc
@system unittest 
{
	Functor v; 
	Action!(Leaf!Ternary, process) a; 
	assert(v == 0); 
	auto res = a(v); 
	assert(v == 0); 
	assert(res == Ternary.no); 
	v = 5; 
	res = a(v, 5); 
	assert(v == 10); 
	assert(res == Ternary.no); 
	res = a(v, 20, 20, 20); 
	assert(v == 70); 
	assert(res == Ternary.yes);
	
	v = 10; 
	res = a(v, 40);
	assert(v == 500);
	assert(res == Ternary.unknown);
}

/**
	The condition node. Executes T with provided arguments, reports the result. The result follows the principle of
	excluded middle.
*/
struct Condition(T) 
{
	T member; 
	
	auto opCall(U...)(auto ref U args) 
	{
		bool res = member(forward!args);
		return Ternary(res);
	}
}

///
@nogc
@system unittest
{
	
	Condition!(Leaf!bool) c; 

	assert(c(42) == Ternary.no); 
	assert(c(73) == Ternary.yes); 
}

/// The selector node. Tries to tick all of its children until one delivers an answer inequal to false. 
struct Selector(T...)
{
	T members; 
	this(T...)(auto ref T m) 
	{
		foreach(i, t; T)
		{
			members[i] = m[i]; 
		}
	}
	auto opCall(U...)(auto ref U args) 
	{
		foreach(t; members)
		{
			auto res = t(forward!args); 
			if(res != Ternary.no)
				return res; 
		}
		return Ternary.no;
	}
}

///
@nogc 
@system unittest
{
	Selector!(
		Condition!(Leaf!bool),
		Action!(Leaf!Ternary, process)
	) sel;
	
	Functor v; 
	v = 10; 
	auto res = sel(v, 30, 40); 
	assert(v == 80);
	assert(res == Ternary.yes); 
	
	v = 10; 
	res = sel(v, 20); 
	assert(v == 500); 
	assert(res == Ternary.unknown); 


	v = 10; 
	res = sel(v, 10); 
	assert(v == 30); 
	assert(res == Ternary.no); 
}

/// The sequence node. Tries to tick all of its children until one delivers an answer equal to false. 
struct Sequence(T...)
{
	T members; 
	this(T...)(auto ref T m) 
	{
		foreach(i, t; T)
		{
			members[i] = m[i]; 
		}
	}
	auto opCall(U...)(auto ref U args) 
	{
		foreach(t; members)
		{
			auto res = t(forward!args); 
			if(res != Ternary.yes)
				return res; 
		}
		return Ternary.yes;
	}
}

///
@nogc
@system unittest
{
	Sequence!(
		Condition!(Leaf!bool),
		Action!(Leaf!Ternary, process)
	) sel;
	
	Functor v; 
	v = 10; 
	auto res = sel(v, 30, 40); 
	assert(v == 150);
	assert(res == Ternary.yes); 
	
	v = 10; 
	res = sel(v, 20); 
	assert(v == 30); 
	assert(res == Ternary.no); 

	v = 50; 
	res = sel(v); 
	assert(v == 500); 
	assert(res == Ternary.unknown); 
}

/**
	The parallel node. Tries to tick all of its children in parallel. Returns true if and only if there were more then
	Y successful ticks. If there were less successful ticks, returns false, if there were more then N failure ticks. 
	Otherwise returns running. 
*/
struct Parallel(T...)
{
	import std.parallelism : parallel; 
	import std.algorithm : filter, count; 

	T members; 
	size_t S;
	size_t F;

	Ternary[T.length] states; 

	this(size_t s, size_t f)
	{
		S = s; 
		F = f; 
	}
	this(T...)(size_t s, size_t f, auto ref T m) 
	{
		S = s; 
		F = f;
		foreach(i, t; T)
		{
			members[i] = m[i]; 
		}
	}
	auto opCall(U...)(auto ref U args)
	{
		foreach(i, t; parallel([members]))
			states[i] = t(forward!args); 
		if(states[].filter!(a => a == Ternary.yes).count >= S)
			return Ternary.yes; 
		if(states[].filter!(a => a == Ternary.no).count >= F)
			return Ternary.no; 
		return Ternary.unknown; 
	}
}

///
//@nogc //no nogc for parallel
@system unittest
{
	Functor v; 
	
	size_t successes = 1; 
	size_t failures = 4; 
	
	auto p = Parallel!(
		Action!(Leaf!Ternary, process),
		Action!(Leaf!Ternary, process),
		Action!(Leaf!Ternary, process),
		Action!(Leaf!Ternary, process)
	)(successes,failures);
	
	assert(p.S == successes); 
	assert(p.F == failures); 
	
	v = 10;
	auto res = p(v, 0); 
	assert(v == 10); 
	assert(res == Ternary.no); 
	
	v = 10; 
	res = p(v, 10); 
	assert(v == 500); 
	assert(res == Ternary.unknown); 
	
	failures = 3; 
	p = Parallel!(
		Action!(Leaf!Ternary, process),
		Action!(Leaf!Ternary, process),
		Action!(Leaf!Ternary, process),
		Action!(Leaf!Ternary, process)
	)(successes,failures);
	assert(p.S == successes); 
	assert(p.F == failures); 
	v = 10; 
	res = p(v, 10); 
	assert(v == 500); 
	assert(res == Ternary.no); 

	v = 10; 
	res = p(v, 15); 
	assert(v == 70); 
	assert(res == Ternary.yes); 
}

/*
	according to "Using Decorators to Improve Behaviors", Alex J. Champandard on July 26, 2007
	decorators can be separated e.g. in 4 main types: 
	Filters, 
	Managers & Handlers
	Control Modifiers
	Meta Operations

	While for filters, passing the evaluated member without any further params could be enough, this is not the case for
	other types of decorators:
	Managers & Handlers have to have the ability to alter the member before acting
	Control Modifiers don't modify the member, but have the ability to call it on its own account
	Meta Operations do not evaluate the member at all. 
*/
/// Decorator node. Reseives the child and its args for fine granular adjustments. 
struct Decorator(T, alias dec) 
{
	T member; 

	static if(__traits(compiles, dec.init)) dec d;
	
	this(T)(auto ref T t) 
	{
		member = t; 
	}
	auto opCall(U...)(auto ref U args)
	{
		static if(isCallable!dec)
		{
			return Ternary(dec(member, forward!args));
		}
		else
		{
			static if(__traits(isTemplate, dec))
				return Ternary(dec!(typeof(member), U)(member, forward!args));
			else
				return Ternary(d(member, forward!args)); 
		}
	}
}

///
@nogc
@system unittest
{
	Action!(Leaf!Ternary, process) a;
	Decorator!(Action!(Leaf!Ternary, process), Not) d1; 

	Functor v; 
	v = 0; 
	auto res = a(v, 10); 
	assert(v == 10); 
	assert(res == Ternary.no); 
	v = 0; 
	res = d1(v, 10); 
	assert(v == 10); 
	assert(res == Ternary.yes); 

	v = 50; 
	res = a(v); 
	assert(v == 500); 
	assert(res == Ternary.unknown); 
	v = 50; 
	res = d1(v); 
	assert(v == 500); 
	assert(res == Ternary.unknown); 

	v = 5; 
	res = a(v, 10, 10, 10, 10, 10); 
	assert(v == 55); 
	assert(res == Ternary.yes); 
	v = 5; 
	res = d1(v, 10, 10, 10, 10, 10); 
	assert(v == 55); 
	assert(res == Ternary.no); 
	
	Condition!(Leaf!bool) c;
	Decorator!(Condition!(Leaf!bool), Not) d2; 
	v = 0; 
	res = c(v, 10); 
	assert(v == 10); 
	assert(res == Ternary.no); 
	v = 0; 
	res = d2(v, 10); 
	assert(v == 10); 
	assert(res == Ternary.yes); 

	v = 5; 
	res = c(v, 10, 10, 10, 10, 10); 
	assert(v == 55); 
	assert(res == Ternary.yes); 
	v = 5; 
	res = d2(v, 10, 10, 10, 10, 10); 
	assert(v == 55); 
	assert(res == Ternary.no); 
}

/// The root node. 
struct Root(T)
{
	T member; 
	this(T)(auto ref T t)
	{
		member = t; 
	}
	auto opCall(U...)(auto ref U args)
	{
		return member(forward!args); 
	}
}

///
@nogc
@system unittest
{
	static assert(
		__traits(
			compiles, Root!(
				Sequence!(
					Action!((Leaf!Ternary), process), 
					Condition!(Leaf!bool), 
					Selector!(
						Decorator!(Condition!(Leaf!bool), Not), 
						Action!((Leaf!Ternary), process)
					)
				)
			)
		)
	);
}